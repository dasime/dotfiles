# dotfiles

Config files for different programs.

## Installing individual files on Windows

Assuming you have checked out this repo to `%USERPROFILE%\dotfiles`
symlinks can be created from an Administrator cmd like so:

```sh
MKLINK "%USERPROFILE%\alias.cmd" "%USERPROFILE%\dotfiles\alias.cmd"
```

or to link a directory:

```sh
MKLINK /D "%USERPROFILE%\.config\git" "%USERPROFILE%\dotfiles\.config\git"
```

## Git Submodules 101

I always forget how to setup submodules in a fresh checkout.

1.  After cloning the repo, you need to initialise the local configuration:

    ```sh
    git submodule init
    ```

2.  Then checkout the code for the submodules:

    ```sh
    git submodule update
    ```

If you want to pull down the latest changes you can update all modules with:

```sh
git submodule update --remote
```

Or, just go into the module you want to update and use normal git commands.
