#!/bin/bash
# Concatenate multiple images horizontally or vertically
#
# Usage
#
# Concatenate images vertically (commands are equivalent)
#
#   icat vertical 001.jpg 002.jpg 003.jpg 004.jpg
#   icat v 00*
#
# Concatenate images horizontally (commands are equivalent)
#
#   icat horizontal 001.jpg 002.jpg 003.jpg 004.jpg
#   icat h 00*
#
# You can also pass in additional processing commands after the file list.
# For example, to resize images to a uniform width:
#
#   icat v 00* -resize 1024x
#
# or to resize to a uniform height
#
#   icat h 00* -resize x576
#

function eof {
  echo 'Correct usage: icat {v[ertical],h[orizontal]} images...'
  exit 1
}

main() {
  case "$1" in
    v*)
      align="-append"
      ;;
    h*)
      align="+append"
      ;;
    *)
      # Invalid or no alignment
      eof
      ;;
  esac

  if [ $# -lt 3 ]; then
    # Need to include multiple images to concat
    eof
  fi

  ext=${2##*.}

  convert \
    $align \
    "${@:2}" \
    "$(date +%Y%m%d_%H%M%S).${ext}"
}

main "$@"
