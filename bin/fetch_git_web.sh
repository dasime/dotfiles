#!/bin/bash
# Attempt to open a git repo's hosted webpage
#
# Works for GitLab and GitHub
#
# Usage
#
#   $ git remote get-url origin
#   git@github.com:jaegertracing/jaeger-operator.git
#   $ git_web
#   Opening https://github.com/jaegertracing/jaeger-operator
#   $ git_web mr
#   Opening https://github.com/jaegertracing/jaeger-operator/pulls
#

case "$OSTYPE" in
   cygwin*)
      launch="cmd /c start"
      ;;
   linux*)
      launch="xdg-open"
      ;;
   darwin*)
      launch="open --url"
      ;;
esac

function main() {
  remote_url_protocol=$(git remote get-url origin)

  remote_url_https=$(sed -En 's|.*@([a-z.-]+):([a-zA-Z\/-]+)(\.git)?|https://\1/\2|p' <<< "$remote_url_protocol")

  case "${1-}" in

    mr|pr)
      if grep -q "gitlab" <<< "$remote_url_https"; then
        # GitLab Merge Requests
        remote_url_https="${remote_url_https}/-/merge_requests"
      elif grep -q "github" <<< "$remote_url_https"; then
        # GitHub Pull Requests
        remote_url_https="${remote_url_https}/pulls"
      fi
      ;;

    issue|issues)
      if grep -q "gitlab" <<< "$remote_url_https"; then
        # GitLab Issues
        remote_url_https="${remote_url_https}/-/issues"
      elif grep -q "github" <<< "$remote_url_https"; then
        # GitHub Issues
        remote_url_https="${remote_url_https}/issues"
      fi
      ;;

  esac

  echo "Opening ${remote_url_https}"

  $launch "$remote_url_https"
}

main "$@"
