#!/bin/bash
# Open the JIRA ticket for the current branch in your browser
#
# Configuration
#
#   # Set your JIRA instance's URL as an environment variable
#   export JIRA_URL='https://ctrlio.atlassian.net'
#
#   # Override the default branch regex with:
#   export JIRA_BRANCH_REGEX='s/.+\/([a-zA-Z0-9]+-[0-9]+).*/\1/p'
#
#   # Override the default issue URL path:
#   export JIRA_ISSUES_URL_PATH="/browse/"
#
# Usage
#
#   $ git rev-parse --abbrev-ref HEAD
#   feature/AB-123-my-work
#   $ jira
#   Opening issue #AB-123
#   $ jira CD-456
#   Opening issue #CD-456
#
# Based on igoradamenko's fork of the dead Oh-My-Zsh Jira plugin
#
# * https://github.com/igoradamenko/jira.plugin.zsh
# * https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/jira
#

# matches a gitflow pattern like: 'feature/AB-123' or 'hotfix/CD-456_helpful-message'
readonly BRANCH_REGEX=${JIRA_BRANCH_REGEX-'s/.+\/([a-zA-Z0-9]+-[0-9]+).*/\1/p'}

# The path to use in issues URLs: defaults to `/browse/`
readonly ISSUES_URL_PATH=${JIRA_ISSUES_URL_PATH-"/browse/"}

case "$OSTYPE" in
   cygwin*)
      launch="cmd /c start"
      ;;
   linux*)
      launch="xdg-open"
      ;;
   darwin*)
      launch="open --url"
      ;;
esac

function _jira_open_from_branch() {
  if [[ $(git rev-parse --is-inside-work-tree > /dev/null 2>&1 ; echo $?) -eq "0" ]]; then
    local branch
    branch=$(git rev-parse --abbrev-ref HEAD)
    local issue
    issue=$(echo "$branch" | sed -nE "$BRANCH_REGEX")

    if [[ -n $issue ]]; then
      _jira_open_issue "$issue"
    else
      echo "Sorry, there is no issue code in the branch name."
    fi
  else
    echo "Sorry, but it's not a Git repository."
  fi
}

function _jira_open_issue() {
  local issue=${1}

  local url="${JIRA_URL}${ISSUES_URL_PATH}"

  echo "Opening issue #${issue}"

  $launch "${url}${issue}"
}

function main() {
  if [[ -n "$1" ]]; then
    _jira_open_issue "$1"
  else
    _jira_open_from_branch
  fi
}

main "$@"
