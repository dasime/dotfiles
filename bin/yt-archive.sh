#!/bin/bash
# YouTube Archive 'yt-dlp' Wrapper
#
# When downloading content from YouTube, sometimes you just want a copy
# but sometimes you actually want an archive.
#
# Set "DOWNLOAD_TARGET" to either a single video or a playlist.
#
# The other "DOWNLOAD_*" options are set to "1" to download or "0" to skip.
# For example, use "DOWNLOAD_AUDIO=1" when the video is not required.
#
# Video is remuxed to "mkv" and audio is remuxed to "mka".
# Remuxing is lossless, this is just to avoid less supported file extensions
# like "movie.webm" or "audio.ogg". Also enables embedding extra data.
#
# The "DOWNLOAD_META" option creates a README that contains the video
# description as well as additional video metadata as frontmatter, including:
#
# * Original title
# * Source URL
# * Channel name and Channel ID
# * Upload date
#
# The below variables should be edited to control the script:

DOWNLOAD_TARGET='https://www.youtube.com/watch?v=jNQXAC9IVRw'

DOWNLOAD_VIDEO=1
DOWNLOAD_AUDIO=0
DOWNLOAD_THUMB=1
DOWNLOAD_META=1

# Prefix for standalone videos
OUTPUT_PREFIX='[%(upload_date>%Y-%m-%d)s] %(title)s [YouTube]/'

# Prefix for YouTube Playlists for albums
#OUTPUT_PREFIX='[2022-09-09] %(playlist_title)s [YouTube]/%(playlist_autonumber)02d - %(title)s - '

#
# End of documentation.
#
## STATUS REPORTING

_LEFT_COL_WIDTH=16

status() {
  FMT_RED='\033[0;31m'
  FMT_GREEN='\033[0;32m'
  FMT_RESET='\033[0m'

  if [ "$2" == 1 ]; then
    printf "%${_LEFT_COL_WIDTH}s ${FMT_GREEN}TRUE${FMT_RESET}\n" "${1}..."
  else
    printf "%${_LEFT_COL_WIDTH}s ${FMT_RED}FLASE${FMT_RESET}\n" "${1}..."
  fi
}

printf "%${_LEFT_COL_WIDTH}s %s\n" 'Downloading:' "$DOWNLOAD_TARGET"

if [ -x "$(command -v yt-dlp)" ]; then
  status 'yt-dlp found' 1
else
  status 'yt-dlp found' 0
  exit 1
fi

status 'Video' $DOWNLOAD_VIDEO
status 'Audio' $DOWNLOAD_AUDIO
status 'Thumbnail' $DOWNLOAD_THUMB
status 'Metadata' $DOWNLOAD_META

## YT-DLP CONTROL

read -r -d '' README_FORMAT <<'EOF'
---
title: >-
  %(title)s
author: >-
  %(channel)s (%(channel_id)s)
date: %(upload_date>%Y-%m-%d)s
source: "%(original_url)s"
---

## Original Description

%(description)s
EOF

if [ "$DOWNLOAD_META" == 1 ]; then
  yt-dlp \
    --no-restrict-filenames \
    --skip-download \
    --print-to-file \
      "$README_FORMAT" \
      "${OUTPUT_PREFIX}README.md" \
    "$DOWNLOAD_TARGET"
fi

if [ "$DOWNLOAD_THUMB" == 1 ]; then
  # If also downloading music, it's 'cover', otherwise it's 'fanart'
  [ "$DOWNLOAD_AUDIO" == 1 ] && FILENAME='cover' || FILENAME='fanart'

  yt-dlp \
    --no-restrict-filenames \
    --skip-download \
    --write-thumbnail \
    --convert-thumbnails 'png' \
    -o "thumbnail:${OUTPUT_PREFIX}${FILENAME}.%(ext)s" \
    "$DOWNLOAD_TARGET"
fi

if [ "$DOWNLOAD_VIDEO" == 1 ]; then
  yt-dlp \
    --no-restrict-filenames \
    --format 'bestvideo*+bestaudio/best' \
    --embed-metadata \
    --sub-langs 'all' \
    --embed-subs \
    --remux-video 'mkv' \
    -o "${OUTPUT_PREFIX}movie.%(ext)s" \
    "$DOWNLOAD_TARGET"
fi

if [ "$DOWNLOAD_AUDIO" == 1 ]; then
  yt-dlp \
    --no-restrict-filenames \
    --format 'bestaudio/best' \
    --embed-metadata \
    --extract-audio \
    --remux-video 'mka' \
    -o "${OUTPUT_PREFIX}audio.%(ext)s" \
    "$DOWNLOAD_TARGET"
fi
