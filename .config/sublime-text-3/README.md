# Sublime Text 3 Settings

Homepage: <https://www.sublimetext.com/>

Install addons using [PackageControl](https://packagecontrol.io/). Installation instructions are available on the PackageControl homepage.

## Config Windows

Files go in the usual place:

    %APPDATA%\Sublime Text 3

which is:

    C:\Users\%USERNAME%\AppData\Roaming\Sublime Text 3

### Config Installation

Delete the existing `User` package if it exists.

    IF EXIST "%APPDATA%\Sublime Text 3\Packages\User" RMDIR /Q /S "%APPDATA%\Sublime Text 3\Packages\User"

Assuming you have checked out this repo to `%USERPROFILE%\dotfiles` the below command will create the symlink.
This must be run from an Administrator cmd session:

    MKLINK /D "%APPDATA%\Sublime Text 3\Packages\User" "%USERPROFILE%\dotfiles\.config\sublime-text-3\Packages\User"

If you get any failures while downloading packages check the console in sublime - there is a good chance
it'll be one of these: <https://packagecontrol.io/docs/troubleshooting#Windows_Errors_12029_and_12057>.

## Config Mac

Files go in the usual place:

    ~/Library/Application\ Support/Sublime\ Text

Note, MacOS will not detect configuration in the `.config` directory.
