# markdownlint

Homepage

* <https://www.npmjs.com/package/markdownlint>
* <https://github.com/DavidAnson/markdownlint>

Commandline Interface

* <https://www.npmjs.com/package/markdownlint-cli>
* <https://github.com/igorshubovych/markdownlint-cli>

SublimeLinter Plugin

* <https://packagecontrol.io/packages/SublimeLinter-contrib-markdownlint>
* <https://github.com/jonlabelle/SublimeLinter-contrib-markdownlint>

## Install on Windows

```sh
MKLINK /D "%USERPROFILE%\.config\markdownlint" "%USERPROFILE%\dotfiles\.config\markdownlint"
npm install -g markdownlint-cli
```
