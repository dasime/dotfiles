#!/usr/env python3
# Store command history for interactive Python shells in a sensible place.
#
# Support 'PYTHON_HISTORY' on Python < 3.13 and update default location
# from `~/.python_history` to `$XDG_STATE_HOME/python/.python_history`.
import atexit
import os
import pathlib
import readline
import sys


def main():
    python_history = os.environ.get("PYTHON_HISTORY")

    _data_home = os.environ.get(
        "XDG_STATE_HOME", os.path.join(os.path.expanduser("~"), ".local/state")
    )
    python_state_path = os.path.join(_data_home, "python")

    histfile = os.path.join(
        python_history or python_state_path,
        ".python_history"
    )

    try:
        readline.read_history_file(histfile)
        h_len = readline.get_current_history_length()
    except FileNotFoundError:
        pathlib.Path(python_state_path).mkdir(mode=0o755, parents=True, exist_ok=True)
        open(histfile, "wb").close()
        h_len = 0

    def save(prev_h_len, histfile):
        new_h_len = readline.get_current_history_length()
        readline.set_history_length(1000)
        readline.append_history_file(new_h_len - prev_h_len, histfile)

    atexit.register(save, h_len, histfile)


if sys.version_info < (3, 13):
    main()
