# Window configuration
border=no

# Enable file-specific configuration
# For example: 'video.avi' will load 'video.avi.conf'
use-filedir-conf=yes

## Reduce default OSD size, the defaults are too big.
osd-font="DejaVu Sans"
osd-font-size=48

# Screenshots
screenshot-format=png
screenshot-template="%F-%P"
screenshot-directory=~/Downloads

# Audio settings
audio-channels=stereo
audio-normalize-downmix=yes
volume-max=100

# Subtitle defaults
slang=jpn,eng
alang=jpn,eng

sub-font="Vesta"
sub-bold=no
sub-font-size=48
sub-margin-y=50
sub-color="#FFEEEE"
sub-border-color="#110000"
sub-border-size=3
sub-shadow-offset=1.2
sub-shadow-color="#33000000"
sub-spacing=0

# Don't open the window smaller than 640x480.
# Useful for opening 176x144 3gp videos on 4k screens.
autofit-smaller=640x480

# Load all subs containing the media filename in the same directory
# For example: 'movie.mp4' will load 'movie.eng.srt'
sub-auto=fuzzy

# Hare+Guu Sub path
sub-file-paths=Subtitles

# new defaults upstream 2023-09-27
# When playing fast-foward, mute audio if the playback speed is above 8
# (previous default was 4)
af=scaletempo2=max-speed=8

# new defaults upstream 2023-09-19
scale=lanczos
cscale=lanczos
dscale=mitchell
dither-depth=auto
correct-downscaling=yes
linear-downscaling=yes
sigmoid-upscaling=yes
hdr-compute-peak=auto # Requires compute shaders, which is a fairly recent OpenGL feature, and will probably also perform horribly on some drivers,
hdr-peak-decay-rate=20
hdr-scene-threshold-low=1.0
hdr-scene-threshold-high=3.0
deband-threshold=48
deband-grain=32

# vr-reversal / 360plugin script for munging VR content
script=~~/vendor/vr-reversal/360plugin.lua

[tessellate]
# Tessellating video experiment
autofit-smaller=950x530
autofit-larger=950x530

# Extension Overrides

[extension.avi]
keep-open=always

[extension.mkv]
keep-open=always

[extension.wmv]
keep-open=always

[extension.mp4]
keep-open=always

[extension.ogm]
# fixes stutters/framedrops in broken OGM files
no-correct-pts

[extension.jpg]
pause

[extension.jpeg]
pause

[extension.png]
pause

[extension.gif]
loop-file=inf

[extension.webm]
loop-file=inf

# Play video in a compatible terminal
[term]
vo=tct
vo-tct-256=yes
really-quiet

# OS Specific configuration
# The associated profile **must** exist in the included file.
# For example, `mpv.linux.conf` should contain a [linux] profile
#   to load the default rules. But can also contain additional auto profiles.
[linux]
include=~~/mpv.linux.conf

[windows]
include=~~/mpv.windows.conf

# FFmpeg Filters can be used to create Visualisations a la Winamp.
# Documentation: https://ffmpeg.org/ffmpeg-filters.html#Multimedia-Filters
# A couple of interesting ones to try:
#
#   * avectorscope (--profile=vis-vector)
#   * showwaves (--profile=vis-waves)
#   * showspectrum (--profile=vis-spectrum)
#
# More discussion here:
#   https://securitronlinux.com/debian-testing/how-to-get-a-nice-waveform-display-with-the-mpv-media-player-on-linux/
#   https://github.com/mpv-player/mpv/issues/7614

[vis-vector]
lavfi-complex="[aid1]asplit[ao][a1];[a1]avectorscope=r=25:m=lissajous_xy:bc=200:gc=100:rc=75:bf=5:gf=3:rf=1:zoom=1,format=rgb0[vo]"

[vis-waves]
lavfi-complex="[aid1]asplit[ao][a]; [a]showwaves[vo]"

[vis-spectrum]
lavfi-complex="[aid1]asplit[ao][a]; [a]showspectrum=color=rainbow:slide=scroll[vo]"
