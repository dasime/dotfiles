-- osprofile.lua
-- Attempt to confirm which operating system we're running on
-- and load the appropriate profile.
--
-- On Linux it will attempt to load the [linux] profile from mpv.conf.
-- Otherwise it will attempt to load the [windows] profile from mpv.conf.
local utils = require 'mp.utils'
local msg = require 'mp.msg'

-- `package.config:sub(1,1)` will return `\\` on Win and `/` on Unix
-- See: https://stackoverflow.com/a/14425862
if package.config:sub(1,1) == '/'
then
  -- Using a profile like this is a bit cludgey but the manual advices against
  -- invoking the include command directly using:
  -- `mp.set_property("include", "~~/mpv.linux.conf")`
  -- See: https://mpv.io/manual/master/#command-interface-include
  msg.info("Attempting to load [linux] profile")
  mp.commandv("apply-profile", "linux")
else
  msg.info("Attempting to load [windows] profile")
  mp.commandv("apply-profile", "windows")
end
