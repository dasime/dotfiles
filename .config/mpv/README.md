# mpv Settings

Homepage: <https://mpv.io/>

See also youtube-dl: <https://rg3.github.io/youtube-dl/index.html>

## Config Linux

Files go in the usual place:

    ~/.config/mpv

## Config Windows

To register the file associations and fix the thumbnails see: <https://github.com/rossy/mpv-install>

Files go in the usual place:

    %APPDATA%\mpv

which is:

    C:\Users\%USERNAME%\AppData\Roaming\mpv

### Config Installation

Delete the existing `mpv` config directory if it exists.

    IF EXIST "%APPDATA%\mpv" RMDIR /Q /S "%APPDATA%\mpv"

Assuming you have checked out this repo to `%USERPROFILE%\dotfiles` the below command will create the symlink.
This must be run from an Administrator cmd session:

    MKLINK /D "%APPDATA%\mpv" "%USERPROFILE%\dotfiles\.config\mpv"
