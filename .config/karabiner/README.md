# Karabiner

_A powerful and stable keyboard customizer for macOS._

Homepage: <https://karabiner-elements.pqrs.org/>

`mine-yabai.json` based on:
<https://github.com/koekeishiya/yabai/pull/268>

## Screenshots and Permissions

To enable running `screencapture` from a Karabiner shortcut
the `karabiner_console_user_server` needs to be manually granted permission.

1.  Open _Security & Privacy_
2.  Select the _Screen Recording_ tab
3.  Click `+` and navigate to:
    `/Library/Application Support/org.pqrs/Karabiner-Elements/bin/`
    Then select: `karabiner_console_user_server`.

NOTE: If either `Karabiner` changes how this functionality works or moves,
you can find the new name of the process by:

1.  Open _Console_
2.  Click _Start Steaming_ to begin capturing new logs
3.  Attempt to screenshot and then click _Pause_
4.  Filter the logs for `tccd`. Review logs for something like:

    ```text
    Notifying for access  kTCCServiceScreenCapture for target PID[111], responsiblePID[222],
    responsiblePath: /Library/Application Support/org.pqrs/Karabiner-Elements/bin/karabiner_console_user_server to UID: 333
    ```
