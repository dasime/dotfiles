# Path to your oh-my-zsh installation.
export ZSH="${XDG_DATA_HOME}/ohmyzsh"

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="brill"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
  kubectl
  vi-mode
)

# User configuration

# export PATH="/home/${USER}/.local/bin:$PATH"
# export MANPATH="/usr/local/man:$MANPATH"

source ${ZSH}/oh-my-zsh.sh

export HISTFILE="${XDG_STATE_HOME}/zsh/history"
[[ ! -d "${XDG_STATE_HOME}/zsh" ]] && mkdir -p "${XDG_STATE_HOME}/zsh"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

DEFAULT_USER=$(whoami)

# Disable `NOMATCH` behaviour.
#
# NOMATCH
# > If a pattern for filename generation has no matches, print an error,
# # >   instead of leaving it unchanged in the argument list.
#
# So many commands include what looks like glob patterns, so I find myself
#   constantly disabling this behaviour. It's easier to keep it off.
setopt +o nomatch

# Switch keybinds to vi mode
bindkey -v

# Up/Down arrow keys should complete current line based on what is already typed
# https://bbs.archlinux.org/viewtopic.php?id=149977
bindkey "^[[A" up-line-or-beginning-search
bindkey "^[[B" down-line-or-beginning-search

# Add support for Operating System Command (OSC): OSC-7 [report CWD]
# Used for reporting CWD to the parent window to enable spawning
# additional terminals in the CWD.
# See: https://codeberg.org/dnkl/foot/wiki#user-content-spawning-new-terminal-instances-in-the-current-working-directory
# See: https://github.com/ohmyzsh/ohmyzsh/pull/9914
function osc7 {
  setopt localoptions extendedglob
  input=( ${(s::)PWD} )
  uri=${(j::)input/(#b)([^A-Za-z0-9_.\!~*\'\(\)-\/])/%${(l:2::0:)$(([##16]#match))}}
  print -n "\e]7;file://${HOSTNAME}${uri}\e\\"
}
add-zsh-hook -Uz chpwd osc7

# Pyenv configuration
if [ -x "$(command -v pyenv)" ]; then
  eval "$(pyenv init -)"
fi

# nvm configuration
[ -s "${NVM_DIR}/nvm.sh" ] && source "${NVM_DIR}/nvm.sh"

export JIRA_URL='https://passfort.atlassian.net'

# git aliases
alias glog="git log --graph --format=oneline-plus --abbrev-commit --date=relative"
alias gloga="git log --graph --format=oneline-plus --abbrev-commit --date=relative --all"
alias glogr="git log --format=md-simple"

gcbr() {
  git checkout -b "$1" "origin/$1"
}

gbc() {
  git push origin "$1" --delete --no-verify && git branch -d "$1"
}

# Remove all remote branches with the supplied pattern.
# Example:
## `gbtidy feature` or `gbtidy release`
gbtidy() {
  git branch -lr | grep "$1" | sed 's:origin/::' | xargs git push --delete --no-verify origin
}

# Find the author of each remote branch
# Useful for reconciling stale branches
gbblame() {
  git for-each-ref --format=' %(authorname) %09 %(refname)' --sort=authorname refs/remotes/
}

alias ayy='echo "lmao"'

alias tf=terraform
alias L=less
alias n=npm

alias csv="column -t -s, -n"
alias tsv="column -t -s $'\t' -n"
alias js="jq . -C"

alias zshrc="vi ${XDG_CONFIG_HOME}/zsh/.zshrc"
alias pls='sudo $(fc -ln -1)'
alias l='ls -laoh'

## global find
gfind() {
  find / -iname $@ 2>/dev/null
}

## local find
lfind() {
  find . -iname $@ 2>/dev/null
}

## Generate a random sha1 value
sha1ran() {
  head -c 200 /dev/urandom | sha1sum | cut -d' ' -f1
}

plf() {
  sed -i -e 's/http:\/\//https:\/\//g' package-lock.json
}

plc() {
  git checkout -- package-lock.json
}

poetrynuke() {
  rm -r "$(poetry env info -p)"
}

# alias xclip to use the default clipboard
alias xclip="xclip -selection c"

# Clean up stopped docker containers and dangling images
dockerprune() {
  docker system prune
}

# Merge a new kubeconfig file with an existing kubectl config.
#
# `KUBECONFIG` is a `:` delimited list of `kubeconfig` files.
# However, in the context of `kmerge`, it only makes sense to operate on a single file.
# To do this, we assume the first file listed in `KUBECONFIG` is the file to merge into.
# All other `kubeconfig` files listed in `KUBECONFIG` are ignored.
#
# When `KUBECONFIG` isn't set, default the target to the `kubectl` default `~/.kube/config`.
#
# Usage:
#   kmerge kubeconfig_environment
#
# Inspiration:
#   https://github.com/kubernetes/kubernetes/issues/46381#issuecomment-461404505
#
kmerge() {
  KUBECONFIG_ROOT=${$(cut -d: -f1 <<< $KUBECONFIG):-"~/.kube/config"}
  KUBECONFIG="$KUBECONFIG_ROOT:$1" \
    kubectl config view --flatten > "${XDG_RUNTIME_DIR}/kubeconfig" \
    && mv "${XDG_RUNTIME_DIR}/kubeconfig" "$KUBECONFIG_ROOT"
}

# Delete all legacy replicasets for the provided namespace.
# Useful for when you're looking at a sea of noise and
# something is on fire.
# Usage:
#   krepnuke observability
krepnuke() {
  kubectl delete $(kubectl get all -n $1 | grep replicaset.apps | grep "0         0         0" | cut -d' ' -f 1) -n $1
}

# Print the date
# ncal : New functionality from the `cal` command
#   -w : Print week number
#   -b : Print weeks horizontally rather than vertically
#   -3 : Print three months, one month either side of current
#   -M : Start weeks on Monday
alias acal="ncal -wb3M"

alias yy="fc -ln -1 | tr -d '\n' | xclip"

alias mvn="mv -n"

# Simple calculations can be simple everywhere!
#
# Usage:
#   $ c 8 * 500
#   4000
#
#   $ c 500 / 8
#   62.5
#
#   You can also optionally quote equations:
#
#   $ c "(512 * 1000**3) / 1024**3"
#   476.837158203125
#
# Although technically possible to use `echo` for this,
# in practice `echo` doesn't properly support decimals.
#
#   $ echo $((8 * 500))
#   4000
#   $ echo $(((460/1.15)*4))
#   1600.0000000000002
#   $ echo "(460/1.15)*4" | bc
#   1600
#
# For a while, I was using `bc` with this alias but the syntax
# is a bit different. In the end, I think Python works best.
c() {
  python3 -c "from math import *;print($*)"
}
# This alias makes it possible to use the `*` literally without the shell
# interpreting it as a glob pattern.
# Without this, any equation containing a `*` must be quoted.
alias c="noglob c"

md5it() {
  for file in "$@"; do
    ext=${file##*.}
    md5sum=$(md5sum "$file" | cut -d' ' -f1 | tr '[:lower:]' '[:upper:]')

    mv "$file" "${md5sum}.${ext}"
  done
}

# print ONLY the name of the current/working directory (no path)
pwd0() {
  basename "$(pwd)"
}

# Use `systemd`'s mount unit as an alternative to `mount`.
# It's become fashionable to not use `setuid` on `mount` because `setuid` is
# scary and `udisks` has largely replaced it for block devices.
# But `udisks` is not suitable for remote drives like SMB shares and on
# immutable filesystems you may not be able to easily or permanently `setuid`.
sdmount() {
  systemctl start "$(systemd-escape -p --suffix=mount "$(readlink -f "$1")")"
}

sdumount() {
  systemctl stop "$(systemd-escape -p --suffix=mount "$(readlink -f "$1")")"
}

# Recursively delete all '.DS_Store' files below current working directory
dspurge() {
  find . -name .DS_Store -type f -delete -print
}
