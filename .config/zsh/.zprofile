# Source minimum env vars
. ~/.config/de/environ

# Style `less`, for use with `man`.
# Notes: https://unix.stackexchange.com/questions/108699/documentation-on-less-termcap-variables
export LESS_TERMCAP_mb=$'\E[01;31m'       # begin blinking
export LESS_TERMCAP_md=$'\E[01;38;5;74m'  # begin bold
export LESS_TERMCAP_me=$'\E[0m'           # end mode
export LESS_TERMCAP_se=$'\E[0m'           # end standout-mode
export LESS_TERMCAP_so=$'\E[7m'           # begin standout-mode - info box
                                          #   (invert current selection)
export LESS_TERMCAP_ue=$'\E[0m'           # end underline
export LESS_TERMCAP_us=$'\E[04;38;5;146m' # begin underline

# Pyenv configuration
export PYENV_ROOT="${XDG_DATA_HOME}/pyenv"
export PATH="${PYENV_ROOT}/bin:${PATH}"

if [ -x "$(command -v pyenv)" ]; then
  eval "$(pyenv init --path)"
fi

# nvm configuration
export NVM_DIR="${XDG_DATA_HOME}/nvm"
# If the only node installed is provided by `nvm` then it needs to be init'd
# for scripts to run outside of interactive terminals
[ -s "${NVM_DIR}/nvm.sh" ] && source "${NVM_DIR}/nvm.sh"

# rvm configuration
# NOTE: This still appears to be broken
export rvm_prefix="${XDG_CONFIG_HOME}/rvm"
export rvm_path="${XDG_DATA_HOME}/rvm"
[ -s "/etc/profile.d/rvm.sh" ] && source "/etc/profile.d/rvm.sh"

# Flatpak --user installed packages:
if [ -x "$(command -v flatpak)" ]; then
  export XDG_DATA_DIRS="${XDG_DATA_HOME}/flatpak/exports/share:${XDG_DATA_DIRS}"
fi

# Add Homebrew software to path
if [ -d "/opt/homebrew/bin" ] ; then
  PATH="/opt/homebrew/bin:${PATH}"
fi

# Add local bin to path
if [ -d "${HOME}/.local/bin" ] ; then
  PATH="${HOME}/.local/bin:${PATH}"
fi

# Add go bin to path
if [ -d "${XDG_DATA_HOME}/go/bin" ] ; then
  PATH="${XDG_DATA_HOME}/go/bin:${PATH}"
fi

# Add rust/cargo bin to path
if [ -d "${XDG_DATA_HOME}/cargo/bin" ] ; then
  PATH="${XDG_DATA_HOME}/cargo/bin:${PATH}"
fi

# Host specific profile file
[ -s "${ZDOTDIR}/.zprofile.d/${HOST}" ] && source "${ZDOTDIR}/.zprofile.d/${HOST}"
