# Path to the completion cache file.
ZSH_COMPDUMP="${XDG_STATE_HOME:-${HOME}/.local/state}/.zcompdump"

# Set ZSH base directory
ZDOTDIR=${XDG_CONFIG_HOME:-${HOME}/.config}/zsh

# Path to the cache folder.
ZSH_CACHE_DIR="${XDG_CACHE_HOME:-${HOME}/.cache}/ohmyzsh"

# ZSH Custom
ZSH_CUSTOM="${XDG_DATA_HOME:-${HOME}/.local/share}/zsh"
