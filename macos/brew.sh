#!/bin/bash

brew install \
  azcopy \
  azure-cli \
  circleci \
  cmctl \
  gnu-sed \
  go \
  google-cloud-sdk \
  hex-fiend \
  imagemagick \
  jq \
  karabiner-elements \
  kitty \
  kustomize \
  md5sha1sum \
  mpv \
  mysql-client@5.7 \
  nikitabobko/tap/aerospace \
  nmap \
  postgresql@14 \
  rclone \
  rustup-init \
  shellcheck \
  sublime-text \
  terraform \
  vim \
  watch

go install github.com/GoogleCloudPlatform/cloud-sql-proxy/v2@latest

# `rustup-init` is the 'rustup' installer and needs to be actually run
rustup-init
