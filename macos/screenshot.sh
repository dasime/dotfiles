#!/bin/bash
#
# Various screenshot operations for MacOS
#
# For this script to run, the binary running the script requires OS permissions:
#
# * To take a screenshot it needs _Screen Recording_ in _Security & Privacy_.
# * To write a screenshot it needs _Automation_ in _Security & Privacy_.
#
# Depending on how you plan to run this, that would be your terminal of choice.
#
# Tools:
#
# * screencapture - Screenshot tool (built-in)
# * osascript - execute OSA scripts (AppleScript, JavaScript, etc.)
#               used to interact with the Mac Pasteboard (built-in)
# * imagemagick - For recombining 'full' screenshots
#
set -eu

# It can be challenging to persist environment variables in MacOS.
# Karabiner doesn't inherit the user PATH
PATH="/opt/homebrew/bin/:${PATH}"

case "${1-}" in

  full)
    # Screenshot everything visible
    #
    # MacOS's screencapture doesn't have a way to just capture
    # everything visible across all displays _in one image_.
    #
    # From the documentation, the only way to capture multiple displays
    # at the same time is to specify a filename for each display:
    #
    # > screencapture [-icMPmwsWxSCUtoa] [files]
    # > files   where to save the screen capture, 1 file per screen
    #
    # So if we want to grab everything visible we have to
    # first attempt to explicitly capture every display
    # then use ImageMagick to concatenate them.
    #
    # Because this script appends images based on their display
    # number if your real world layout puts '2' to the left of '1'
    # then it'll appear the wrong way around in the output.

    # First create a temporary directory for this screenshot and intermediates
    FILENAME_BASE=$(date +%Y%m%d_%H%M%S)
    SCREENSHOT_TMPDIR="/private/tmp/$(id -u)/${FILENAME_BASE}"
    mkdir -p -m 0700 "$SCREENSHOT_TMPDIR"

    # Try and capture up to nine screens
    #  -x     do not play sounds
    screencapture -x "${SCREENSHOT_TMPDIR}"/{01..09}.png

    convert \
      +append "${SCREENSHOT_TMPDIR}"/* \
      "${SCREENSHOT_TMPDIR}/${FILENAME_BASE}.png"

    osascript -e \
      "set the clipboard to ¬
        (read ¬
          (POSIX file \"${SCREENSHOT_TMPDIR}/${FILENAME_BASE}.png\") ¬
          as «class PNGf» ¬
        )"
    ;;

  select)
    # Prompt for a region to screenshot
    #  -c     force screen capture to go to the clipboard
    #  -s     only allow mouse selection mode
    #  -x     do not play sounds
    screencapture -tpng -csx
    ;;

  window)
    # Screenshot focused window
    #  -c     force screen capture to go to the clipboard
    #  -o     in window capture mode, do not capture the shadow of the window
    #  -w     only allow window selection mode
    #  -x     do not play sounds
    screencapture -tpng -cowx
    ;;

  save)
    # Save contents of clipboard
    FILENAME=$(date +%Y%m%d_%H%M%S).png
    readonly FILENAME
    osascript -e \
      "tell application \"System Events\" to ¬
        write (the clipboard as «class PNGf») to ¬
          (make new file at folder \"$(pwd)\" ¬
            with properties {name:\"${FILENAME}\"})" || return $?
    echo "Writing to $(pwd)/${FILENAME}"
    ;;

  *)
    # Invalid or no option
    echo 'Correct usage: screenshot-macos {full,select,window,save}'
    exit 1
    ;;

esac
