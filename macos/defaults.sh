#!/bin/bash

# References:
#
# * https://github.com/galvanist/dotfiles
# * https://github.com/SixArm/macos-defaults
# * https://macos-defaults.com
# * https://github.com/sqrthree/defaults-write
# * https://github.com/dirtymouse/defaultswrite
# * https://github.com/mathiasbynens/dotfiles

# Close any open System Preferences panes, to prevent them from overriding
# settings we’re about to change
osascript -e 'tell application "System Preferences" to quit'

## Input

# Disable "natural" scrolling.
defaults write NSGlobalDomain com.apple.swipescrolldirection -bool false

# Disable Bottom Right screen corner hot corner (defaults to Notes.app)
defaults write com.apple.dock wvous-br-corner -int 0

## Input : Trackpad

# Enable tap to click for this user and for the login screen
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
sudo defaults write com.apple.AppleMultitouchTrackpad Clicking 1

# map bottom right corner to right-click
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadCornerSecondaryClick -int 2
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadRightClick -bool true
defaults -currentHost write NSGlobalDomain com.apple.trackpad.trackpadCornerClickBehavior -int 1
defaults -currentHost write NSGlobalDomain com.apple.trackpad.enableSecondaryClick -bool true

## Input : Keyboard

# Disable automatic capitalization.
defaults write NSGlobalDomain NSAutomaticCapitalizationEnabled -bool false

# Disable automatic period substition a.k.a. "smart stops".
# defaults write NSGlobalDomain NSAutomaticPeriodSubstitutionEnabled -int 0
defaults write NSGlobalDomain NSAutomaticPeriodSubstitutionEnabled -bool false

# Disable automatic dash substitution e.g. "smart dashes".
defaults write NSGlobalDomain NSAutomaticDashSubstitutionEnabled -bool false

# Disable automatic quote substitution a.k.a. "smart quotes".
defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false

# Disable automatic spelling correction a.k.a. "auto-correct".
defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false

# Enable full keyboard access for all controls
# (e.g. enable Tab in modal dialogs)
defaults write NSGlobalDomain AppleKeyboardUIMode -int 3

# Stop iTunes from responding to the keyboard media keys
launchctl unload -w /System/Library/LaunchAgents/com.apple.rcd.plist 2> /dev/null

## Output

# Enable subpixel font rendering on non-Apple LCDs.
# See animation: https://github.com/kevinSuttle/macOS-Defaults/issues/17#issuecomment-266633501
defaults write NSGlobalDomain AppleFontSmoothing -int 1

# Disable beeping when things happen.
defaults write NSGlobalDomain com.apple.sound.beep.feedback -int 0
defaults write NSGlobalDomain com.apple.sound.uiaudio.enabled -int 0

## OS : Accessibility

# Show all file extensions.
defaults write NSGlobalDomain AppleShowAllExtensions -bool true

# Expand save panel by default.
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true

# Disable animations when opening and closing windows.
defaults write NSGlobalDomain NSAutomaticWindowAnimationsEnabled -bool false

# Increase window resize speed for Cocoa applications.
defaults write NSGlobalDomain NSWindowResizeTime -float 0.001

## OS : Locale

# Set language and text formats
defaults write NSGlobalDomain AppleLanguages -array "en"
defaults write NSGlobalDomain AppleLocale -string "en_GB@currency=GBP"
defaults write NSGlobalDomain AppleMeasurementUnits -string "Centimeters"
defaults write NSGlobalDomain AppleMetricUnits -bool true

# Set the timezone; see `sudo systemsetup -listtimezones` for other values
sudo systemsetup -settimezone "Europe/London" > /dev/null

## OS : Misc

# Don't maintain app state between sessions.
defaults write com.apple.loginwindow TALLogoutSavesState -bool false

# Don't relaunch apps on boot.
defaults write com.apple.loginwindow LoginwindowLaunchesRelaunchApps -bool false

# Show battery life as a percentage.
# TODO: Doesn't work
defaults write com.apple.menuextra.battery ShowPercent -string "YES"

# Disable automatic termination of inactive apps
defaults write NSGlobalDomain NSDisableAutomaticTermination -bool true

## OS : Screenshots

# Set where to write screenshots.
defaults write com.apple.screencapture location -string "$HOME/Downloads"

# Save screenshots in PNG format (other options: BMP, GIF, JPG, PDF, TIFF)
defaults write com.apple.screencapture type -string "png"

# Disable shadow in screenshots
defaults write com.apple.screencapture disable-shadow -bool true

## OS : Unused functionality

# Save to disk (not to iCloud) by default.
defaults write NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool false

# Disable built-in captive portal browser - use default browser instead
sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.captive.control Active -bool false

# Disable Notification Center and remove the menu bar icon
launchctl unload -w /System/Library/LaunchAgents/com.apple.notificationcenterui.plist 2> /dev/null

# Disable timemachine. There are better solutions for purely work devices.
sudo tmutil disable

## Programs : Dock

# Reduce whitespace in the menu bar
# https://flaky.build/native-fix-for-applications-hiding-under-the-macbook-pro-notch
defaults -currentHost write -globalDomain NSStatusItemSelectionPadding -int 6
defaults -currentHost write -globalDomain NSStatusItemSpacing -int 6

# Do not animate opening applications from the Dock.
defaults write com.apple.dock launchanim -bool false

# Make Dock icons of hidden applications translucent.
defaults write com.apple.dock showhidden -bool true

# Remove the "recently used apps" section from the Dock.
defaults write com.apple.dock show-recents -bool false

# Show indicator lights for open applications in the Dock.
defaults write com.apple.dock show-process-indicators -bool true

# Automatically hide and show the Dock.
defaults write com.apple.dock autohide -bool true

# Remove the delay before opening Dock.
defaults write com.apple.Dock autohide-delay -float 0

# Remove the Dock opening and closing animation times.
defaults write com.apple.dock autohide-time-modifier -float 0

# Disable expose animation.
defaults write com.apple.dock expose-animation-duration -float 0

# Don't rearrange Spaces automatically.
defaults write com.apple.dock mru-spaces -bool false

# Disable the Launchpad gesture (pinch with thumb and three fingers)
defaults write com.apple.dock showLaunchpadGestureEnabled -int 0

## Programs : Spotlight

# Change indexing order and disable some search results
defaults write com.apple.spotlight orderedItems -array \
    '{"enabled" = 1;"name" = "APPLICATIONS";}' \
    '{"enabled" = 1;"name" = "SYSTEM_PREFS";}' \
    '{"enabled" = 0;"name" = "DOCUMENTS";}' \
    '{"enabled" = 0;"name" = "DIRECTORIES";}' \
    '{"enabled" = 0;"name" = "PDF";}' \
    '{"enabled" = 0;"name" = "PRESENTATIONS";}' \
    '{"enabled" = 0;"name" = "SPREADSHEETS";}' \
    '{"enabled" = 0;"name" = "MESSAGES";}' \
    '{"enabled" = 0;"name" = "CONTACT";}' \
    '{"enabled" = 0;"name" = "EVENT_TODO";}' \
    '{"enabled" = 0;"name" = "BOOKMARKS";}' \
    '{"enabled" = 0;"name" = "IMAGES";}' \
    '{"enabled" = 0;"name" = "MOVIES";}' \
    '{"enabled" = 0;"name" = "MUSIC";}' \
    '{"enabled" = 0;"name" = "FONTS";}' \
    '{"enabled" = 0;"name" = "SOURCE";}'

# Disable spotlight indexing (we're not using it to find files)
sudo mdutil -i off / > /dev/null

## Programs : Finder

# Enable quitting finder
defaults write com.apple.finder QuitMenuItem -bool true

# Hide desktop icons (don't give that directory any special treatment)
defaults write com.apple.finder CreateDesktop -bool false

# Don't write '.DS_Store' to network mounts
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool TRUE

## Programs : Terminal

# Only use UTF-8 in Terminal.app
defaults write com.apple.terminal StringEncodings -array 4

# Disable Secure Keyboard Entry in Terminal.app
# It gets in the way of custom keyboard shortcuts
defaults write com.apple.terminal SecureKeyboardEntry -bool true

# Kill running programs to restart them
killall Dock Finder
