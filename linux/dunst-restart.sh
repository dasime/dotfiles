#!/bin/bash
set -eu

killall dunst; notify-send "Dunst Restarted" "Dunst has been restarted."
