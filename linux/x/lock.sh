#!/bin/bash
set -eu

# Fetch the background colour from xrdb (probably Xresources)
background_colour=$(xrdb -query | grep -Po '(?<=background:\s)#[0-9a-f]{6}')

i3lock -i ~/.local/share/de/wallpaper_full.png -t -c "$background_colour" -f
