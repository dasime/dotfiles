#!/bin/bash
#
# A script to manage extending the desktop to external displays using xrandr
#
set -eu

readonly PRIMARY='eDP1'

# Auto config DPI/refresh, add panning offset as required.
xrandr_common(){
  echo " --auto --panning 3840x2160+$1+$2"
}

main() {
  local xrandr_command
  xrandr_command="xrandr --output ${PRIMARY} $(xrandr_common 0 0)"

  local UHD
  UHD=false

  echo 'Current adapter status is:'
  xrandr --query | grep -o '.*connected' | awk '{print "  *",$0}'
  echo ''

  echo 'Which adapter status would you like to modify?'
  select adapter in 'DP1' 'DP2' 'Cancel'; do
    case ${adapter} in
      DP1) xrandr_command+=' --output DP1'; break ;;
      DP2) xrandr_command+=' --output DP2'; break ;;
      Cancel) exit ;;
    esac
  done

  echo 'What state/mode should this adapter be set to?'
  select mode in 'Disabled' '1080p' '1440p' '4k' 'Cancel'; do
    case ${mode} in
      Disabled)
        xrandr_command+="$(xrandr_common 0 0) --off";
        echo "Executing: '${xrandr_command}'";
        ${xrandr_command};
        exit
        ;;
      1080p)
        # FHD
        xrandr_command+="$(xrandr_common 3840 0) --mode 1920x1080 --scale 2x2";
        break
        ;;
      1440p)
        # QHD
        xrandr_command+="$(xrandr_common 3840 0) --mode 2560x1440 --scale 1.5x1.5";
        break
        ;;
      4k)
        # UHD
        xrandr_command+="$(xrandr_common 3840 0) --mode 3840x2160";
        UHD=true;
        break
        ;;
      Cancel)
        exit ;;
    esac
  done

  if [[ "${UHD}" = true ]]; then
    # The big 4k TV in the office has some overscan issues
    #   when it's connected via the Apple HDMI-->TB3 dongle
    echo 'Enable manual overscan correction using Transform?'
    select overscan in 'Yes' 'No' 'Cancel'; do
    case ${overscan} in
      Yes)
        xrandr_command+=' --transform 1.025,0,-50,0,1.025,-30,0,0,1';
        break
        ;;
      No)
        break ;;
      Cancel)
        exit ;;
      esac
    done
  fi

  ## I drive i3wm entirely with the keyboard so the screen positioning doesn't
  ##   really matter. We might as well just short circuit this step.
  # echo 'Is the screen to the left or right of the laptop?'
  # select pos in 'Left' 'Right' 'Cancel'; do
  #   case ${pos} in
  #     Left) xrandr_command+=" --left-of ${PRIMARY}"; break;;
  #     Right) xrandr_command+=" --right-of ${PRIMARY}"; break;;
  #     Cancel) exit;;
  #   esac
  # done
  xrandr_command+=" --right-of ${PRIMARY}"

  echo "Executing: '${xrandr_command}'"
  ${xrandr_command}
}

main "$@"
