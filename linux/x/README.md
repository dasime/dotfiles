# X11

```shell
ln -s $(pwd)/.xinitrc ~/
ln -s $(pwd)/display-manager.sh ~/.local/bin/dm
ln -s $(pwd)/lock-suspend.sh ~/.local/bin/lock-suspend
ln -s $(pwd)/lock.sh ~/.local/bin/lock
ln -s $(pwd)/screenshot.sh ~/.local/bin/screenshot
```
