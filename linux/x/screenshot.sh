#!/bin/bash
#
# Various screenshot operations for X11
#
# Tools:
#
# * scrot - Screenshot tool
# * xclip - Interact with the X11 Clipboard
#
set -eu

case "${1-}" in

  full)
    # Screenshot everything visible
    scrot --exec 'xclip -i -selection c -t image/png $f && rm $f'
    ;;

  select)
    # Prompt for a region to screenshot
    scrot --exec 'xclip -i -selection c -t image/png $f && rm $f' --select
    ;;

  window)
    # Screenshot focused window
    scrot --exec 'xclip -i -selection c -t image/png $f && rm $f' --focused
    ;;

  save)
    # Save contents of clipboard
    readonly OUTFILE=$(pwd)/$(date +%Y%m%d_%H%M%S).png
    xclip -o -selection c -t image/png > "$OUTFILE"
    echo "Writing to $OUTFILE"
    ;;

  *)
    # Invalid or no option
    echo 'Correct usage: screenshot-x {full,select,window,save}'
    exit 1
    ;;

esac
