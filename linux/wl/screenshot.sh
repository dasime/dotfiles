#!/bin/bash
#
# Various screenshot operations
#
# Tools:
#
# * grim - Screenshot tool
# * slurp - Wayland region selection
# * swaymsg - Interact with Sway
# * jq - JSON parsing Sway data
# * wl-copy - Interact with Wayland clipboard
# * convert - ImageMagick transform utility (only used for colour)
#
# Reference:
#
# https://git.sr.ht/~emersion/grim/
#
set -eu

# Screenshot everything visible
if [[ "$1" = "full" ]]; then
  grim - | wl-copy -t image/png
fi

# Prompt for a region to screenshot
if [[ "$1" = "select" ]]; then
  grim -g "$(slurp)" - | wl-copy -t image/png
fi

# Screenshot focused window
if [[ "$1" = "window" ]]; then
  grim -g "$(swaymsg -t get_tree | jq -j '.. | select(.type?) | select(.focused).rect | "\(.x),\(.y) \(.width)x\(.height)"')" - | wl-copy -t image/png
fi

# Screenshot focused display
if [[ "$1" = "display" ]]; then
  grim -o $(swaymsg -t get_outputs | jq -r '.[] | select(.focused) | .name') - | wl-copy -t image/png
fi

# Print the colour of a specific pixel
if [[ "$1" = "colour" ]]; then
  grim -g "$(slurp -p)" -t ppm - | convert - -format '%[pixel:p{0,0}]' txt:- | wl-copy
fi

# Save contents of clipboard
if [[ "$1" = "save" ]]; then
  readonly OUT=$(pwd)/$(date +%Y%m%d_%k%M%S).png
  wl-paste > "$OUT"
  echo "Screenshot saved to $OUT"
fi
