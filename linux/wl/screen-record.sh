#!/bin/bash
#
# Various screen recording operations
#
# Tools:
#
# * wf-recorder - Wayland screen recording tool
# * slurp - Wayland region selection
# * swaymsg - Interact with Sway
# * jq - JSON parsing Sway data
#
# Usage:
#
# Capture region:
#   screen-record select
#
# Capture window to './foo.mp4'
#   screen-record window './foo.mp4'

set -eu

readonly OUTPUT_TARGET=${2-"${XDG_RUNTIME_DIR}/$(date +%Y%m%d_%H%M%S).mp4"}

case "${1-}" in

  full)
    # screen-record everything visible
    wf-recorder \
      -f "$OUTPUT_TARGET"
    ;;

  select)
    # Prompt for a region to screen-record
    wf-recorder \
      -f "$OUTPUT_TARGET" \
      -g "$(slurp)"
    ;;

  window)
    # screen-record focused window
    wf-recorder  \
      -f "$OUTPUT_TARGET" \
      -g "$(swaymsg -t get_tree | jq -j '.. | select(.type?) | select(.focused).rect | "\(.x),\(.y) \(.width)x\(.height)"')"
    ;;

  save)
    # Save contents of clipboard
    echo "Not implemented. Writes to '${XDG_RUNTIME_DIR}' by default."
    exit 1
    ;;

  *)
    # Invalid or no option
    echo 'Correct usage: screen-record {full,select,window}'
    exit 1
    ;;

esac
