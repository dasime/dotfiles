#!/bin/bash
#
# Toggle WiFi depending on eth (enx9cebe87aa2e2) state
# From: https://wiki.archlinux.org/index.php/NetworkManager#Use_dispatcher_to_automatically_toggle_wireless_depending_on_LAN_cable_being_plugged_in
# From: https://superuser.com/a/367472
#
# This script needs to be copied to `/etc/NetworkManager/dispatcher.d/`
#   and chowned to root:root. NM is very particular about running scripts.

set -eu

wired_interfaces="en.*|eth.*"

if [[ "$1" =~ $wired_interfaces ]]; then
    case "$2" in
        up)
            nmcli radio wifi off
            ;;
        down)
            nmcli radio wifi on
            ;;
    esac
fi
