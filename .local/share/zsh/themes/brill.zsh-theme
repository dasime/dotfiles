# autoload -U colors && colors
# Disable automatic Python venv Prompt
export VIRTUAL_ENV_DISABLE_PROMPT=1

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[yellow]%}git=%{$fg[green]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX=""
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%}*%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN=""

# mysql prompt
# NOTE: '\u' is escaped with it's octal character value '\165' to avoid
# the character being flagged as the start of a unicode sequence
export MYSQL_PS1=$(printf '\033[36m[\033[33m//\033[32m\\\165\033[36m@\033[33m\\h\033[36m/\\d]\033[0m>\\_')

local PROMPT_USER
# Check the UID
if [[ $UID -ne 0 ]]; then # normal user
  PROMPT_USER='$'
else # root
  PROMPT_USER='#'
fi

local PROMPT_HOST
# Determine if there is value in presenting a host
if [[ -n "$SSH_CLIENT" || -n "$SSH2_CLIENT" ]]; then
  # SSH
  PROMPT_HOST='%{$fg[yellow]%}//%M/%{$fg[cyan]%}'
elif [[ -r '/run/.containerenv' ]]; then
  # podman / toolbox
  PROMPT_HOST="%{$fg[yellow]%}//$(sed -nr 's_name="(.*)"_\1_p' /run/.containerenv)/%{$fg[cyan]%}"
elif [[ -e '/.dockerenv' ]]; then
  # docker (NOTE: `.dockerenv` is an empty file)
  PROMPT_HOST='%{$fg[yellow]%}//%M/%{$fg[cyan]%}'
else
  # local, normal
  PROMPT_HOST=''
fi

query_build() {
  # detect Git branch
  local GIT_PROMPT
  if [ "$(git_current_branch)" ]; then
    GIT_PROMPT="$(git_prompt_info)"
  fi

  # detect Python venv
  local VENV_PROMPT
  if [[ -n "${VIRTUAL_ENV}" ]]; then
    # Poetry still makes a nicer 'VIRTUAL_ENV_PROMPT' even when the prompt
    # is disabled. Fallback to building from 'VIRTUAL_ENV' for other venvs
    local VENV_NAME=${VIRTUAL_ENV_PROMPT:-${VIRTUAL_ENV:t:gs/%/%%}}
    VENV_PROMPT="%{$fg[yellow]%}venv=%{$fg[green]%}${VENV_NAME}%{$reset_color%}"
  fi

  # detect TF workspace
  local TF_PROMPT
  if [[ -d '.terraform' && -r '.terraform/environment' ]]; then
    # check if in terraform dir and file exists
    local TF_WORKSPACE="$(< '.terraform/environment')"
    TF_PROMPT="%{$fg[yellow]%}tf=%{$fg[green]%}${TF_WORKSPACE}%{$reset_color%}"
  fi

  # build query
  local PROMPT_QUERY
  for i in $GIT_PROMPT $TF_PROMPT $VENV_PROMPT; do
    PROMPT_QUERY="$PROMPT_QUERY${PROMPT_QUERY:+"%{$fg[cyan]%}&%{$reset_color%}"}$i"
  done

  if [ "$PROMPT_QUERY" != "" ]; then
    echo "?${PROMPT_QUERY}"
  fi
}
local query_prompt='$(query_build)'

PROMPT="%{$fg[cyan]%}[${PROMPT_HOST}%~${query_prompt}% %{$fg[cyan]%}]%(?.%{$fg[green]%}.%{$fg[red]%})%B${PROMPT_USER}%b "
