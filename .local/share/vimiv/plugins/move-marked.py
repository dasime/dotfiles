"""Move all marked images to target.

Based on `karlch`'s comment here:
<https://github.com/karlch/vimiv-qt/issues/366#issuecomment-823842689>
"""
import os
import shutil

from vimiv import api


@api.commands.register()
def move_marked(target: str):
    """Move all marked images to target."""
    target = os.path.abspath(os.path.expanduser(target))
    if not os.path.isdir(target):
        raise api.commands.CommandError("target must be a directory")

    for src in api.mark.paths:
        shutil.move(src, target)

    api.mark.mark_clear()
