@echo off
:: Inspired by https://stackoverflow.com/questions/20530996/aliases-in-windows-command-prompt#21040825
:: Install using `alias.reg`

:: Temporary system path at cmd startup

set PATH=%PATH%;%LOCALAPPDATA%\bin;%APPDATA%\Python\Python38\Scripts

DOSKEY /MACROFILE=%USERPROFILE%\alias.mac
