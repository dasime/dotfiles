# Windows

Assuming you have checked out this repo to `%USERPROFILE%\dotfiles`
symlinks can be created from an Administrator cmd like so:

```shell
MKLINK "%USERPROFILE%\alias.cmd" "%USERPROFILE%\dotfiles\win\alias.cmd"
MKLINK "%USERPROFILE%\alias.mac" "%USERPROFILE%\dotfiles\win\alias.mac"
if not exist "%LOCALAPPDATA%\bin\" mkdir "%LOCALAPPDATA%\bin"
```

`alias.cmd` adds the directory `%LOCALAPPDATA%\bin` to the path,
shortcuts to programs can be added there to add them to the path.
